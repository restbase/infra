# Infrastructure files & Docker images

This repository contains infrastructure reates files and base docker images for restbase.

## Docker images

* Angular base image: `registry.gitlab.com/restbase/infra/angular`
* Puppeteer base image: `registry.gitlab.com/restbase/infra/puppeteer`
